<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\crud;

class CrudSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Crud::create([
            'nama'=>'sinalabidin',
            'hp'=>'083150711499',
            'gmail'=>'sinalabidin7373',
        ]);
    }
}
