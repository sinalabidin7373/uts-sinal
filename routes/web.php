<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CrudController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

route::get('data',[CrudController::class,'index']);
route::get('data/{id}',[CrudController::class,'show']);
route::get('buat',[CrudController::class,'create']);
route::post('simpan',[CrudController::class,'store']);
route::get('edit/{id}',[CrudController::class,'edit']);
route::post('update/{id}',[CrudController::class,'update']);
route::get('delete/{id}',[CrudController::class,'destroy']);