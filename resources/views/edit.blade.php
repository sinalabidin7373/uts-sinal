<form action="{{ url('update', $edit->id) }}" method="post">
    {{ csrf_field() }}
    <table border="5px" class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
        <tr>
            <th>Nama :</th>
            <td>
                <input type="text" name="nama" id="nama" value="{{ $edit->nama }}">
            </td>
        </tr>
        <tr>
            <th>hp :</th>
            <td>
                <input type="text" name="hp" id="hp" value="{{ $edit->hp }}">
            </td>
        </tr>
        <tr>
            <th>gmail :</th>
            <td>
                <input type="text" name="gmail" id="gmail" value="{{ $edit->gmail }}">
            </td>
        </tr>
        <tr>
            <td>
                <a href="/data">Kembali</a>
            </td>
            <td>
                <button>EDIT</button>
            </td>
        </tr>
    </table>
</form>